package com.shao.demo.elasticjoblitedemo;

import com.cxytiandi.elasticjob.annotation.EnableElasticJob;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableElasticJob
@SpringBootApplication
public class ElasticJobLiteDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ElasticJobLiteDemoApplication.class, args);
    }
}
