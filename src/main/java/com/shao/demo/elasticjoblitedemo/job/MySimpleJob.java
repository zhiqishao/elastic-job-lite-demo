package com.shao.demo.elasticjoblitedemo.job;

import com.cxytiandi.elasticjob.annotation.ElasticJobConf;
import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;
import lombok.extern.slf4j.Slf4j;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author zhiqi.shao
 * @Date 2018/6/12 17:37
 */
@Slf4j
@ElasticJobConf(name = "MySimpleJob1" ,cron = "0/10 * * * * ?")
public class MySimpleJob implements SimpleJob {

    @Override
    public void execute(ShardingContext context) {
        String shardParamter = context.getShardingParameter();
        log.info("分片参数：" + shardParamter);
        switch (context.getShardingItem()) {
            case 0:
                // do something by sharding item 0
                log.info("分片参数：" + 0);
                break;
            case 1:
                log.info("分片参数：" + 1);
                // do something by sharding item 1
                break;
            case 2:
                log.info("分片参数：" + 2);
                // do something by sharding item 2
                break;
            // case n: ...
        }
        /*int value = Integer.parseInt(shardParamter);
        for (int i = 0; i < 100; i++) {
            if (i % 2 == value) {
                String time = new SimpleDateFormat("HH:mm:ss").format(new Date());
                log.info(time + ":开始执行简单任务" + i);
            }
        }*/
    }


}
